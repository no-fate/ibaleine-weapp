const baseImageUrl = 'http://www.icjs.ink/ibaleine/mall'

export default{
  logo: '',
  goods1: `${baseImageUrl}/goods_1.jpg`,
  goods2: `${baseImageUrl}/goods_2.jpg`,
  goods3: `${baseImageUrl}/goods_3.jpg`,
  goods4: `${baseImageUrl}/goods_4.jpg`,
  classify1: `${baseImageUrl}/classify_1.png`,
  classify2: `${baseImageUrl}/classify_2.png`,
  classify3: `${baseImageUrl}/classify_3.png`,
  classify4: `${baseImageUrl}/classify_4.png`,
}